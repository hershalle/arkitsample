//
//  ModelCoordinator.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 22/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation


struct ModelCoordinator: Coordinator {
    var rootviewController: UIViewController
    
    enum CameraType {
        case structure
        case arKit
    }
    
    let cameraType: CameraType = .structure
    
    func start(show: (UIViewController) -> Void) {
        
        let modelViewController = R.storyboard.main.instantiateInitialViewController()!
        
        switch cameraType {
        case .arKit:
            let arKitCameraViewController = R.storyboard.arKitCamera.instantiateInitialViewController()!
            modelViewController.arCameraController = arKitCameraViewController
            show(modelViewController)
            
        case .structure:
            
            let depthCameraBatteryIndicatorView = setupDepthCameraBatteryIndicatorView(on: modelViewController)
            
//            let structureCameraManager = StructureCameraManager()
            
            let strctureCameraViewController = R.storyboard.structureCamera.instantiateInitialViewController()!
//            strctureCameraViewController.structureCameraManager = structureCameraManager
            strctureCameraViewController.depthCameraBatteryIndicatorView = depthCameraBatteryIndicatorView
            
            modelViewController.arCameraController = strctureCameraViewController
            
            show(modelViewController)
        }
    }
    
    func setupDepthCameraBatteryIndicatorView(on vc: UIViewController) -> DepthCameraBatteryIndicatorView {
        let depthCameraBatteryIndicatorView = DepthCameraBatteryIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 15))
        
        let navigationRightButton = UIBarButtonItem(customView: depthCameraBatteryIndicatorView)
        vc.navigationItem.rightBarButtonItem = navigationRightButton
        
        return depthCameraBatteryIndicatorView
    }
}
