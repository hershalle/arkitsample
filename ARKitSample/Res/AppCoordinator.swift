//
//  AppCoordinator.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 22/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

struct AppCoordinator: Coordinator {
    var rootviewController: UIViewController
    
    func start(show: (UIViewController) -> Void) {
        show(rootviewController)
        
        let modelCoordinator = ModelCoordinator(rootviewController: rootviewController)
        modelCoordinator.start { (vc) in
            rootviewController.show(vc, sender: self)
        }
    }
    
}
