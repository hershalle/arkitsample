//
//  UIView+Extensions.swift
//  Astralink
//
//  Created by Bar Ziony on 19/07/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import UIKit

extension UIView {

    // MARK: - Auto Layout Helpers

    @discardableResult
    func constraintToEdges(of view: UIView, priority: Float = 1000) -> [NSLayoutConstraint] {
        func edgeConstraint<T>(_ anchor1: NSLayoutAnchor<T>, _ anchor2: NSLayoutAnchor<T>) -> NSLayoutConstraint {
            let constraint = anchor1.constraint(equalTo: anchor2)
            constraint.priority = UILayoutPriority(priority)
            constraint.identifier = "constraint-to-edges"
            return constraint
        }

        let constraints = [
            edgeConstraint(self.topAnchor, view.topAnchor),
            edgeConstraint(self.leftAnchor, view.leftAnchor),
            edgeConstraint(self.bottomAnchor, view.bottomAnchor),
            edgeConstraint(self.rightAnchor, view.rightAnchor)
        ]

        NSLayoutConstraint.activate(constraints)

        return constraints
    }
    
    func addSubviewWithSameSizeConstrainst(subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
        let constraints = [
            subview.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            subview.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            subview.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            subview.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}

