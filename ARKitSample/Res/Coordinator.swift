//
//  Coordinator.swift
//  Astralink
//
//  Created by Bar Ziony on 14/06/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

protocol Coordinator {
    var rootviewController: UIViewController { get }

    func start(show: (UIViewController) -> Void)
    func stop(hide: (UIViewController) -> Void)
}

extension Coordinator {
    func start(show: (UIViewController) -> Void) {
        show(rootviewController)
    }

    func stop(hide: (UIViewController) -> Void) {
        hide(rootviewController)
    }
}
