//
//  Matrix+Extensions.swift
//  Astralink
//
//  Created by Ido Schragenheim on 26/11/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import Foundation

extension GLKMatrix4 {

    var array: [Float] {
        return [m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33]
    }
}
