//
//  ARFlow.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 17/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import CoreMedia
import GLKit
import SwiftyTimer

protocol ARFlowDelegate: class {
    func arFlow(_ arFlow: ARFlow, didChangeState state: ARFlow.State)
}

class ARFlow {
    enum State: String {
        case firstAlignment
        case tracking
        case notActive
    }
    
    weak var delegate: ARFlowDelegate?
    unowned var arCameraController: ARCameraController
    
    init(arCameraController: ARCameraController) {
        self.arCameraController = arCameraController
    }
    
    private(set) var state: State = .notActive {
        didSet {
            guard state != oldValue else {
                return
            }
            
            switch state {
            case .notActive:
                break
            case .firstAlignment:
                Timer.after(20.seconds) {
                    self.state = .tracking
                }
            case .tracking:
                break
            }
            
            delegate?.arFlow(self, didChangeState: state)
        }
    }
    
    func start() {
        state = .firstAlignment
    }
}

//extension ARFlow: ARCameraControllerDelegate {
//    func arCamera(_ arCamera: ARCameraController, didUpdate tracking: GLKMatrix4) {
//        // pass to viewer
//    }
//
//    func arCamera(_ arCamera: ARCameraController, didUpdate sampleBuffer: CMSampleBuffer) {
//        // use on structure first alignment
//    }
//}

