//
//  ARCameraViewController.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 17/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import ARKit

class ARKitCameraViewController: ARCameraController {
    
    @IBOutlet private var sceneView: ARSCNView!
    private var arSCNViewManager: ARSCNViewManager!
    private var didInterrupt = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubviewWithSameSizeConstrainst(subview: sceneView)
        arSCNViewManager = ARSCNViewManager(sceneView: sceneView)
        arSCNViewManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if didInterrupt {
            didInterrupt = false
            arSCNViewManager.start()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        didInterrupt = true
        arSCNViewManager.pause()
    }
    
    override func start() {
        arSCNViewManager.start()
    }
    
    override func pause() {
        arSCNViewManager.pause()
    }
}

extension ARKitCameraViewController: ARSCNViewManagerDelegate {
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate trackingTransform: matrix_float4x4) {
        func glkVector4(from float4: simd_float4) -> GLKVector4 {
            return GLKVector4Make(float4.x, float4.y, float4.z, float4.w)
        }
        
        func glkMatrix4(from float4x4: matrix_float4x4) -> GLKMatrix4 {
            let columns = trackingTransform.columns
            return GLKMatrix4MakeWithColumns(glkVector4(from: columns.0), glkVector4(from: columns.1), glkVector4(from: columns.2), glkVector4(from: columns.3))
        }
        
        let glkMatrix = glkMatrix4(from: trackingTransform)
        delegate?.arCamera(self, didUpdate: glkMatrix)
    }
    
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate sampleBuffer: CMSampleBuffer) {
        delegate?.arCamera(self, didUpdate: sampleBuffer)
    }
    
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate state: ARSCNViewManager.State) {
        func message(from state: ARSCNViewManager.State) -> String {
            
            let message: String
            switch state {
            case .trackingNormalWithAnchors:
                message = "tracking normal"
            case .trackingNormalWithoutAnchors:
                message = "Move the device around to detect horizontal surfaces."
            case .trackingNotAvailable:
                message = "Tracking unavailable."
            case .trackingIsLimitedBecouseExcessiveMotion:
                message = "Tracking limited - Move the device more slowly."
            case .trackingIsLimitedBecouseInsufficientFeatures:
                message = "Tracking limited - Point the device at an area with visible surface detail, or improve lighting conditions."
            case .initializing:
                message = "Initializing AR session."
            case .sessionFail(_):
                message = "Session Failed - probably due to lack of camera access"
            case .sessionInterrupted:
                message = "Session interrupted"
            case .sessionResumed:
                message = "Session resumed"
            }
            
            return "ARKit: " + message
        }
        
        print(message(from: state))
    }
}




