//
//  ARSceneView.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 26/12/2017.
//  Copyright © 2017 Shai Balassiano. All rights reserved.
//

import ARKit

protocol ARSCNViewManagerDelegate: class {
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate trackingTransform: matrix_float4x4)
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate state: ARSCNViewManager.State)
    func arSCNViewController(_ arSCNViewController: ARSCNViewManager, didUpdate sampleBuffer: CMSampleBuffer)
}

class ARSCNViewManager: NSObject {
    
    enum State {
        case trackingNormalWithAnchors
        case trackingNormalWithoutAnchors
        case trackingNotAvailable
        case trackingIsLimitedBecouseExcessiveMotion
        case trackingIsLimitedBecouseInsufficientFeatures
        case initializing
        case sessionFail(error: Error)
        case sessionInterrupted
        case sessionResumed
    }
    
    private var startTransform: matrix_float4x4?
    weak var delegate: ARSCNViewManagerDelegate?
    var shouldSendSampleBuffer = false
    
    private weak var sceneView: ARSCNView!
    init(sceneView: ARSCNView) {
        super.init()
        
        self.sceneView = sceneView
        setupSceneView()
    }
    
    private func setupSceneView() {
        sceneView.delegate = self
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        sceneView.showsStatistics = true
        sceneView.session.delegate = self
    }
    
    private func sceneConfiguration() -> ARConfiguration {
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravityAndHeading
        configuration.planeDetection = .horizontal
        return configuration
    }
    
    func start() {
        let configuration = sceneConfiguration()
        sceneView.session.run(configuration)
        sceneView.session.delegate = self
    }
    
    func pause() {
        sceneView.session.pause()
    }
    
    func startSendingTrackingFromCurrentPostion() {
        startTransform = sceneView.session.currentFrame?.camera.transform
    }
    
    private func resetTracking() {
        let configuration = sceneConfiguration()
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
}

extension ARSCNViewManager: ARSCNViewDelegate {
    private func state(from trackingState: ARCamera.TrackingState, in frame: ARFrame) -> State {
        let state: State
        
        switch trackingState {
        case .normal where frame.anchors.isEmpty:
            state = .trackingNormalWithoutAnchors
        case .normal:
            state = .trackingNormalWithAnchors
        case .notAvailable:
            state = .trackingNotAvailable
        case .limited(.excessiveMotion):
            state = .trackingIsLimitedBecouseExcessiveMotion
        case .limited(.insufficientFeatures):
            state = .trackingIsLimitedBecouseInsufficientFeatures
        case .limited(.initializing):
            state = .initializing
        }
        
        return state
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        guard let currentFrame = session.currentFrame else {
            return
        }
        
        let state = self.state(from: camera.trackingState, in: currentFrame)
        delegate?.arSCNViewController(self, didUpdate: state)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        delegate?.arSCNViewController(self, didUpdate: State.sessionFail(error: error))
        resetTracking()
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        delegate?.arSCNViewController(self, didUpdate: State.sessionInterrupted)
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        delegate?.arSCNViewController(self, didUpdate: State.sessionResumed)
        resetTracking()
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        func constructPlaneNode(planeAnchor: ARPlaneAnchor) -> SCNNode {
            let planeGeometry = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
            let planeNode = SCNNode(geometry: planeGeometry)
            planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
            
            //SCNPlane is vertically oriented in its local coordinate space, so rotate the plane to match the horizontal orientation of `ARPlaneAnchor`.
            planeNode.eulerAngles.x = -.pi / 2
            
            // Make the plane visualization semitransparent to clearly show real-world placement.
            planeNode.opacity = 0.25
            
            return planeNode
        }
        
        guard let planeAnchor = anchor as? ARPlaneAnchor else {
            return
        }
        
        let planeNode = constructPlaneNode(planeAnchor: planeAnchor)
        node.addChildNode(planeNode)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            guard let planeAnchor = anchor as? ARPlaneAnchor, let planeNode = self.sceneView.node(for: anchor)?.childNodes.first, let planeGeometry = planeNode.geometry as? SCNPlane else {
                return
            }
            
            func update(planNode: SCNNode, from planeAnchor: ARPlaneAnchor) {
                // Plane estimation may shift the center of a plane relative to its anchor's transform.
                planeNode.simdPosition = float3(planeAnchor.center.x, 0, planeAnchor.center.z)
                
                //Plane estimation may extend the size of the plane, or combine previously detected planes into a larger one. In the latter case, `ARSCNView` automatically deletes the corresponding node for one plane, then calls this method to update the size of the remaining plane.
                planeGeometry.width = CGFloat(planeAnchor.extent.x)
                planeGeometry.height = CGFloat(planeAnchor.extent.z)
            }
            
            update(planNode: planeNode, from: planeAnchor)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        DispatchQueue.main.async {
            for childNode in node.childNodes {
                childNode.removeFromParentNode()
            }
        }
    }
}

extension ARSCNViewManager: ARSessionDelegate {
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        func descriptionFormat(from pixelBuffer: CVPixelBuffer) -> CMVideoFormatDescription {
            var videoFormatDescription: CMVideoFormatDescription?
            CMVideoFormatDescriptionCreateForImageBuffer(kCFAllocatorDefault, pixelBuffer, &videoFormatDescription)
            return videoFormatDescription!
        }

        func sampleBuffer(from frame: ARFrame, resizeToVGA: Bool = false) -> CMSampleBuffer {
            struct StaticHolder {
                static let resizer = CVPixelBufferResize()
            }
            
            var pixelBuffer = frame.capturedImage
            
            if resizeToVGA {
                pixelBuffer = StaticHolder.resizer.processCVPixelBuffer(pixelBuffer).takeRetainedValue()
            }
            
            let videoFormatDescription = descriptionFormat(from: pixelBuffer)
            
            // the number of zeros equal to the number of digits after the floting point. We want value / scale == frame.timestamp (seconds)
            let scale = CMTimeScale(NSEC_PER_SEC)
            let value = Int64(frame.timestamp * Double(scale))
            let presentationTimeStamp = CMTime(value: CMTimeValue(value), timescale: scale)
            var timingInfo = CMSampleTimingInfo(duration: kCMTimeInvalid, presentationTimeStamp: presentationTimeStamp, decodeTimeStamp: kCMTimeInvalid)
            
            var sampleBuffer: CMSampleBuffer?
            CMSampleBufferCreateReadyWithImageBuffer(kCFAllocatorDefault, pixelBuffer, videoFormatDescription, &timingInfo, &sampleBuffer)
            return sampleBuffer!
        }

        func sendSampleBufferToDelegate() {
            guard shouldSendSampleBuffer else {
                return
            }
            
            let newSampleBuffer = sampleBuffer(from: frame, resizeToVGA: true)
            DispatchQueue.main.async {
                self.delegate?.arSCNViewController(self, didUpdate: newSampleBuffer)
            }
        }
        
        func sendTrackingToDelegate() {
            DispatchQueue.main.async {
                guard let startTransform = self.startTransform,
                    let currentCameraTransform = self.sceneView.session.currentFrame?.camera.transform else {
                        return
                }
                
                let differenceInTracking = simd_inverse(startTransform) * currentCameraTransform
                self.delegate?.arSCNViewController(self, didUpdate: differenceInTracking)
            }
        }
        
        sendSampleBufferToDelegate()
        sendTrackingToDelegate()
    }
}
