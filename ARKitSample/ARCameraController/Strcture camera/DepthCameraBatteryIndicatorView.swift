//
//  SensorBatteryView.swift
//  Astralink
//
//  Created by Ido Schragenheim on 18/09/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//
import UIKit

class DepthCameraBatteryIndicatorView: UIView {

    var batteryView: BatteryView!
    var titleLabel: UILabel!
    var percentageLabel: UILabel!

    var batteryLevel: Int = -1 {
        didSet {
            batteryView.level = batteryLevel
            if batteryLevel >= 0 {
                percentageLabel.text = "\(batteryLevel)%"
            }
        }
    }

    override init(frame: CGRect) {
        batteryView = BatteryView(frame: frame)
        titleLabel = UILabel(frame: frame)
        percentageLabel = UILabel(frame: frame)
        super.init(frame: frame)
        setupView()
    }

    required convenience init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupView() {
        self.addSubview(titleLabel)
        self.addSubview(batteryView)
        self.addSubview(percentageLabel)

        batteryView.direction = .maxXEdge
        batteryView.backgroundColor = .clear

        titleLabel?.text = "Sensor"
        titleLabel.font = UIFont.systemFont(ofSize: 11)
        titleLabel.adjustsFontSizeToFitWidth = true
        percentageLabel.text = "%"
        percentageLabel.font = UIFont.systemFont(ofSize: 11)

        batteryView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        percentageLabel.translatesAutoresizingMaskIntoConstraints = false

        let subviewsConstraints = [
            titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 36),
            batteryView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            batteryView.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 4),
            batteryView.widthAnchor.constraint(equalToConstant: 28),
            batteryView.heightAnchor.constraint(equalToConstant: 12),
            percentageLabel.leadingAnchor.constraint(equalTo: batteryView.trailingAnchor, constant: 4),
            percentageLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            percentageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ]

        NSLayoutConstraint.activate(subviewsConstraints)
    }

}

