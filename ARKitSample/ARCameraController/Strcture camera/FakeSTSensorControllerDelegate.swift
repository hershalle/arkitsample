//
//  FakeSTSensorControllerDelegate.swift
//  Astralink
//
//  Created by Ido Schragenheim on 24/10/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import Foundation

class FakeSTSensorControllerDelegate: NSObject, STSensorControllerDelegate {

    static let shared = FakeSTSensorControllerDelegate()

    private override init() {
        super.init()
    }

    func sensorDidConnect() {}
    func sensorDidDisconnect() {}
    func sensorDidStopStreaming(_ reason: STSensorControllerDidStopStreamingReason) {}
    func sensorDidLeaveLowPowerMode() {}
    func sensorBatteryNeedsCharging() {}

    func sensorDidOutputSynchronizedDepthFrame(_ depthFrame: STDepthFrame!, colorFrame: STColorFrame!) {
        NSLog("[FakeSTSensorDelegate] sensor did output synced frames got called")
    }

}
