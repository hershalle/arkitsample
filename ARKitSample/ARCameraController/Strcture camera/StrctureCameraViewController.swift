//
//  StrctureCameraViewController.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 17/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

class StrctureCameraViewController: ARCameraController {
    private var timer: Timer?
    
    private var cameraViewController: AVCameraViewController!
    
    // injected
    var depthCameraBatteryIndicatorView: DepthCameraBatteryIndicatorView?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cameraViewController = segue.destination as? AVCameraViewController {
            self.cameraViewController = cameraViewController
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StructureDepthCamera.shared.delegate = self
        arFlow.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startUpdatingDepthCameraBatteryIndicatorView(timeInterval: 5.seconds)
        cameraViewController.cameraService.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopUpdatingDepthCameraBatteryIndicatorView()
    }
    
    
    override func start() {
        cameraViewController.cameraService.start()
        StructureDepthCamera.shared.startStreaming()
        arFlow.start()
    }
    
    override func pause() {
        StructureDepthCamera.shared.pauseStreaming()
    }
    
    // ============================================
    // MARK: - UpdatingDepthCameraBatteryIndicator:
    // ============================================
    private func updateDepthCameraBatteryIndicatorView() {
        depthCameraBatteryIndicatorView?.batteryLevel =  StructureDepthCamera.shared.batteryChargePercentage
    }
    
    private func startUpdatingDepthCameraBatteryIndicatorView(timeInterval: TimeInterval) {
        stopUpdatingDepthCameraBatteryIndicatorView()
        updateDepthCameraBatteryIndicatorView()
        timer = Timer.every(timeInterval, { [weak self] in
            self?.updateDepthCameraBatteryIndicatorView()
        })
    }
    
    func stopUpdatingDepthCameraBatteryIndicatorView() {
        timer?.invalidate()
        timer = nil
    }
}

extension StrctureCameraViewController: AVCameraServiceDelegate {
    func aVCameraService(_ aVCameraService: AVCameraService, didOutput sampleBuffer: CMSampleBuffer) {
//        if arFlow.state == .firstAlignment {
            StructureDepthCamera.shared.syncWithDeviceCamera(sampleBuffer: sampleBuffer)
//        }
    }
}

extension StrctureCameraViewController: StructureDepthCameraDelegate {
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didExtractSyncedFrames frameImage: UIImage, depthInMillimeters: UnsafeMutablePointer<Float>!) {
        
        print("boom")
    }
    
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didUpdateStatus status: StructureDepthCamera.ConnectionStatus) {
        
        func notification(for status: StructureDepthCamera.ConnectionStatus) -> NotificationData? {
            let notification: NotificationData?
            switch status {
            case .connected:
                notification = delegate?.arCameraCurrentNotification(self)
                notification?.icon = #imageLiteral(resourceName: "rounded-checked-icon")
            case .failedBackgroundNotSupported:
                notification = NotificationData(header: "Sensor cannot work in background mode",
                                                body: "",
                                                icon: #imageLiteral(resourceName: "rounded-info-icon"))
            case .failedToConnect:
                notification = NotificationData(header: "Sensor failed to connect to the iPad",
                                                body: "Try to wait for 2 minutes.",
                                                icon: #imageLiteral(resourceName: "rounded-info-icon"))
            case .failedToFind, .disconnected:
                notification = NotificationData(header: "Please connect the sensor to the iPad",
                                                body: "",
                                                icon: #imageLiteral(resourceName: "rounded-info-icon"))
            case .preparing:
                NSLog("sensor is waking up...")
                notification = nil
            }
            
            return notification
        }
        
        if status == .connected {
            updateDepthCameraBatteryIndicatorView()
        }
        
        if let notification = notification(for: status) {
            delegate?.arCamera(self, show: notification, isSticky: status != .connected)
        }
    }
    
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, batteryChargePercentage: Int, isLow: Bool) {
        
        if isLow {
            let notification = NotificationData(header: "Sensor battery is empty",
                                                body: "Please charge the sensor.",
                                                icon: #imageLiteral(resourceName: "rounded-info-icon"))
            
            delegate?.arCamera(self, show: notification, isSticky: true)
        } else {
            delegate?.arCameraHideStickyNotification(self)
        }
    }
    
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didInterruptWithStatus status: StructureDepthCamera.ConnectionStatus) {
        NSLog("structureDepthCamera: didInterruptWithStatus")
        // TODO
    }
}

extension StrctureCameraViewController: ARFlowDelegate {
    func arFlow(_ arFlow: ARFlow, didChangeState state: ARFlow.State) {
        NSLog("\(state)")
        
        switch state {
        case .firstAlignment:
            break
        case .tracking:
            break
        case .notActive:
            break
        }
    }
}
