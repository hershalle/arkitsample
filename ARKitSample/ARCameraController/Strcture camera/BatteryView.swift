//
//  BatteryView.swift
//  Show a battery oriented toward ''direction', charged ''level'' percent.
//  Turns red when level drops below ''lowThreshold''.
//
//  Created by Ido Schragenheim on 18/09/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//
import UIKit

class BatteryView: UIView {

    // MARK: - Behavior Properties
    /// 0 to 100 percent full, unavailable = -1
    var level: Int = -1                     { didSet {layoutLevel()} }

    /// change color when level crosses the threshold
    var lowThreshold: Int = 15              { didSet {layoutFillColor()} }

    var mediumThreshold: Int = 60

    // MARK: - Appearance Properties
    /// direction of battery terminal
    var direction: CGRectEdge = .minYEdge   { didSet {setNeedsLayout()} }

    /// simplified direction of battery terminal (for Interface Builder)
    var isVertical: Bool {
        get {return direction == .maxYEdge || direction == .minYEdge}
        set {direction = newValue ? .minYEdge : .maxXEdge}
    }

    // relative size of  battery terminal
    var terminalLengthRatio: CGFloat = 0.1  { didSet {setNeedsLayout()} }
    var terminalWidthRatio:  CGFloat = 0.4  { didSet {setNeedsLayout()} }

    var highLevelColor: UIColor = UIColor(red: 0.0, green: 0.9, blue: 0.0, alpha: 1) { didSet {layoutFillColor()} }
    var mediumLevelColor: UIColor = UIColor(red: 1.00, green: 0.57, blue: 0.16, alpha: 1.00) { didSet {layoutFillColor()} }
    var lowLevelColor: UIColor  = UIColor(red: 0.9, green: 0.0, blue: 0.0, alpha: 1) { didSet {layoutFillColor()} }
    var noLevelColor: UIColor   = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1) { didSet {layoutFillColor()} }

    /// set as 0 for default borderWidth = length / 20
    var borderWidth: CGFloat = 1.0

    /// set as 0 for default cornerRadius = length / 10
    var cornerRadius: CGFloat = 0 { didSet {layoutCornerRadius()} }

    // MARK: - Overrides
    override var backgroundColor: UIColor? { didSet {layoutFillColor()} }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUp()
    }

    override func layoutSubviews() {
        layoutBattery()
        layoutLevel()
    }

    // MARK: - Sublayers
    private var bodyOutline = CALayer()
    private var terminalOutline = CALayer()
    private var terminalOpening = CALayer()
    private var levelFill = CALayer()

    private func setUp() {
        layer.addSublayer(levelFill)
        layer.addSublayer(bodyOutline)
        layer.addSublayer(terminalOutline)
        layer.addSublayer(terminalOpening)
        setNeedsLayout()
    }

    // MARK: - Layout
    private var length: CGFloat {return isVertical ? bounds.height : bounds.width}

    private func layoutBattery() {
        // divide total length into body and terminal
        let terminalLength = terminalLengthRatio * length
        var (terminalFrame, bodyFrame) = bounds.divided(atDistance: terminalLength, from: direction)

        // layout body
        bodyOutline.frame = bodyFrame
        bodyOutline.borderWidth = borderWidth != 0 ? borderWidth : length / 20

        // layout terminal
        let parallelInsetRatio = (1-terminalWidthRatio) / 2
        let perpendicularInset = bodyOutline.borderWidth
        var (dx, dy) = isVertical ? ( parallelInsetRatio * bounds.width, -perpendicularInset ) : ( -perpendicularInset, parallelInsetRatio * bounds.height )
        terminalFrame = terminalFrame.insetBy(dx: dx, dy: dy)
        (_, terminalFrame) = terminalFrame.divided(atDistance: perpendicularInset, from: direction)
        terminalOutline.frame = terminalFrame
        terminalOutline.borderWidth = bodyOutline.borderWidth

        // cover terminal opening
        var (_, coverFrame) = terminalFrame.divided(atDistance: perpendicularInset, from: direction)
        (dx, dy) = isVertical ? (perpendicularInset, -0.25) : (-0.25, perpendicularInset)
        coverFrame = coverFrame.insetBy(dx: dx, dy: dy)
        terminalOpening.frame = coverFrame
        terminalOpening.backgroundColor = noLevelColor.cgColor

        // layout empty levelFill
        levelFill.frame = bodyFrame.insetBy(dx: perpendicularInset, dy: perpendicularInset).integral
        levelFill.backgroundColor = noLevelColor.cgColor
    }

    private func layoutLevel() {
        var levelFrame = bodyOutline.frame.insetBy(dx: bodyOutline.borderWidth, dy: bodyOutline.borderWidth)
        if level >= 0 && level <= 100 {
            let levelInset = (isVertical ? levelFrame.height : levelFrame.width) * CGFloat(100-level) / 100
            (_, levelFrame) = levelFrame.divided(atDistance: levelInset, from: direction)
        }
        levelFill.frame = levelFrame.integral
        layoutCornerRadius()
        layoutFillColor()
    }

    private func layoutFillColor() {
        if level >= 0 && level <= 100 {
            if level <= lowThreshold {
                levelFill.backgroundColor = lowLevelColor.cgColor
            } else if level <= mediumThreshold {
                levelFill.backgroundColor = mediumLevelColor.cgColor
            } else {
                levelFill.backgroundColor = highLevelColor.cgColor
            }

            terminalOpening.backgroundColor = (backgroundColor ?? .white).cgColor
        }
        else {
            levelFill.backgroundColor = noLevelColor.cgColor
            terminalOpening.backgroundColor = noLevelColor.cgColor
        }
    }

    private func layoutCornerRadius() {
        bodyOutline.cornerRadius = cornerRadius != 0 ? cornerRadius : length / 10
        terminalOutline.cornerRadius = bodyOutline.cornerRadius / 2
    }
}


