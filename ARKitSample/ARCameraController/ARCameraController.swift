//
//  ARCameraController.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 17/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import GLKit
import CoreMedia

protocol ARCameraControllerDelegate: class {
    func arCamera(_ arCamera: ARCameraController, didUpdate tracking: GLKMatrix4)
    func arCamera(_ arCamera: ARCameraController, didUpdate sampleBuffer: CMSampleBuffer)
    func arCamera(_ arCamera: ARCameraController, show notification: NotificationData, isSticky: Bool)
    func arCameraHideStickyNotification(_ arCamera: ARCameraController)
    func arCameraCurrentNotification(_ arCamera: ARCameraController) -> NotificationData?
}

class ARCameraController: UIViewController {
    private(set) var arFlow: ARFlow!
    
    weak var delegate: ARCameraControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arFlow = ARFlow(arCameraController: self)
    }
    
    func start() {}
    func pause() {}
}
