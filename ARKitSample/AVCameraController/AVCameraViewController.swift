//
//  AVCameraViewController.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 23/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

class AVCameraViewController: UIViewController {
    
    let cameraService = AVCameraService()
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraService.configure()
        previewLayer = cameraService.previewLayerInstance()
        view.layer.addSublayer(previewLayer!)
        previewLayer?.frame = view.frame
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        previewLayer?.frame = view.frame
    }
}
