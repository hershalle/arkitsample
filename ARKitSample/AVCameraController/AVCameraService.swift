//
//  AVCameraViewController.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 23/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import UIKit

protocol AVCameraServiceDelegate: class {
    func aVCameraService(_ aVCameraService: AVCameraService, didOutput sampleBuffer: CMSampleBuffer)
}

class AVCameraService: NSObject {
    private let captureSession = AVCaptureSession()
    private var rearCameraInput: AVCaptureInput?
    private var captureDevice: AVCaptureDevice?
    
    weak var delegate: AVCameraServiceDelegate?
    
    func authorizationStatus() -> AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatus(for: .video)
    }
    
    func configure() {
        captureSession.beginConfiguration()
        
        let session = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .unspecified)
        
        let captureOutputData = AVCaptureVideoDataOutput()
        let captureSessionQueue = DispatchQueue(label: "CameraSessionQueue", attributes: [])
        captureOutputData.setSampleBufferDelegate(self, queue: captureSessionQueue)
        
        for captureDevice in session.devices where captureDevice.position == .back {
            self.captureDevice = captureDevice
            self.rearCameraInput = try! AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(self.rearCameraInput!)
            captureSession.addOutput(captureOutputData)
        }
        
        captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
        captureSession.commitConfiguration()
    }
    
    func start() {
//        guard authorizationStatus() == .authorized else {
//            print("unauthorized")
//            return
//        }
        lockColorCamera(exposure: false, whiteBalance: false, andFocus: true)
        captureSession.startRunning()
    }
    
    func pause() {
        
    }
    
    func avCaptureVideoOrientation (from deviceOrientation: UIDeviceOrientation) -> AVCaptureVideoOrientation {
        switch deviceOrientation {
        case .landscapeLeft:
            return .landscapeLeft
        case .landscapeRight:
            return .landscapeRight
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .unknown, .faceUp, .faceDown:
            return .landscapeRight
        }
    }
    
    func previewLayerInstance() -> AVCaptureVideoPreviewLayer {
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = avCaptureVideoOrientation(from: UIDevice.current.orientation)
        return previewLayer
    }
    
    private struct CameraConfig {
        static let useManualExposureAndAutoISO: Bool = true
        static let targetExposureTimeInSeconds: Double = 1.0/60
        static let outputDataVideoSettings: [String:Int] = [kCVPixelBufferPixelFormatTypeKey as String: Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        static let lensPosition: Float = 0.75
    }
    
    func lockColorCamera(exposure shouldLockExposure: Bool,
                         whiteBalance shouldLockWhitBalance: Bool,
                         andFocus shouldLockFocus: Bool) {
        do {
            try captureDevice?.lockForConfiguration()
        } catch let error as NSError {
            NSLog("[ERROR] Failed to lock video device configuration, error: \(error.localizedDescription)")
        }
        
        /// If the manual exposure option is enabled, we've already locked exposure permanently, so do nothing here.
        if shouldLockExposure {
            if CameraConfig.useManualExposureAndAutoISO {
                /// locks the video device to 1/60th of a second exposure time
                setManualExposureAndAutoISO(videoDevice: captureDevice!,
                                            exposureTimeInSec: CameraConfig.targetExposureTimeInSeconds)
                
            } else {
                NSLog("Locking Camera Exposure")
                
                /// Exposure locked to its current value.
                if captureDevice!.isExposureModeSupported(.locked) {
                    captureDevice?.exposureMode = .locked
                }
            }
            
        } else {
            NSLog("Unlocking Camera Exposure")
            /// Auto-exposure
            captureDevice?.exposureMode = .continuousAutoExposure
        }
        
        /// Lock in the white balance here
        if shouldLockWhitBalance {
            /// White balance locked to its current value.
            if captureDevice!.isWhiteBalanceModeSupported(.locked) {
                captureDevice?.whiteBalanceMode = .locked
            }
        } else {
            /// Auto-white balance.
            captureDevice?.whiteBalanceMode = .continuousAutoWhiteBalance
        }
        
        /// Lock focus
        if shouldLockFocus {
            /// Set focus at the 0.75 to get the best image quality for mid-range scene
            captureDevice?.setFocusModeLocked(lensPosition: CameraConfig.lensPosition, completionHandler: nil)
            
        } else {
            captureDevice?.focusMode = .continuousAutoFocus
        }
        captureDevice?.unlockForConfiguration()
    }
    
    private func setManualExposureAndAutoISO(videoDevice: AVCaptureDevice, exposureTimeInSec: Double) {
        var targetExposureTime = CMTimeMakeWithSeconds(exposureTimeInSec, 1000)
        let currentExposureTime = videoDevice.exposureDuration
        let exposureFactor = CMTimeGetSeconds(currentExposureTime) / exposureTimeInSec
        
        let minExposureTime = videoDevice.activeFormat.minExposureDuration
        
        if( CMTimeCompare(minExposureTime, targetExposureTime) > 0 /* means Time1 > Time2 */ ) {
            /// if minExposure is longer than targetExposure, increase our target
            targetExposureTime = minExposureTime
        }
        
        let currentISO: Float = videoDevice.iso
        var targetISO: Float = Float(currentISO) * Float(exposureFactor)
        let maxISO: Float = videoDevice.activeFormat.maxISO
        let minISO: Float = videoDevice.activeFormat.minISO
        
        /// Clamp targetISO to [minISO ... maxISO]
        targetISO = targetISO > maxISO ? maxISO : targetISO < minISO ? minISO : targetISO
        
        videoDevice.setExposureModeCustom(duration: targetExposureTime, iso: targetISO, completionHandler: nil)
    }
}

extension AVCameraService: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        delegate?.aVCameraService(self, didOutput: sampleBuffer)
    }
}
