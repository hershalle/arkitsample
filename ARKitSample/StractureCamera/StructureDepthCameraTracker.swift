//
//  StructureDepthCameraTracker.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 21/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

protocol StructureDepthCameraTrackerDelegate: class {
    func trackerPoseChanged(newPose: GLKMatrix4)
}

class StructureDepthCameraTracker {
    
    weak var delegate: StructureDepthCameraTrackerDelegate?
    
    private var trackerThread: StructureDepthCameraTrackerThread?
    private var initialTrackerPose: GLKMatrix4 = GLKMatrix4Identity
    private let timeoutSeconds: Double = 20/1000.0
    
    private(set) var initialTrackerPoseIsSet: Bool = false
    
    deinit {
        stop()
    }
    
    func start() {
        if trackerThread == nil {
            trackerThread = StructureDepthCameraTrackerThread()
            trackerThread?.delegate = self
        }
        trackerThread?.setInitialTrackerPose(pose: initialTrackerPose, timestamp: CACurrentMediaTime())
        trackerThread?.start()
    }
    
    func pause() {
        //TODO: Implement
    }
    
    func stop() {
        trackerThread?.stop()
        trackerThread = nil
        initialTrackerPoseIsSet = false
    }
    
    func updateWithMotionData(_ motion: CMDeviceMotion) {
        trackerThread?.updateWith(motion: motion)
    }
    
    func updateWithSyncedFrames(depthFrame: STDepthFrame, colorFrame: STColorFrame) {
        trackerThread?.updateWith(depthFrame: depthFrame, andColorFrame: colorFrame, maxWaitTimeInSec: timeoutSeconds)
    }
    
    func setInitialPose(pose: GLKMatrix4) {
        NSLog("setting initial pose to: \(pose.array)")
        initialTrackerPose = pose
        initialTrackerPoseIsSet = true
    }
}

extension StructureDepthCameraTracker: StructureDepthCameraTrackerThreadDelegate {
    func trackerPoseChanged(newPose: GLKMatrix4) {
        NSLog("[Tracker] Tracker pose changed")
        delegate?.trackerPoseChanged(newPose: newPose)
    }
}
