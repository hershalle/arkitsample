//
//  StructureCamera.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 21/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

protocol StructureDepthCameraDelegate: class {
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didUpdateStatus status: StructureDepthCamera.ConnectionStatus)
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, batteryChargePercentage: Int, isLow: Bool)
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didInterruptWithStatus status:StructureDepthCamera.ConnectionStatus)
    
    func structureDepthCamera(_ structureDepthCamera: StructureDepthCamera, didExtractSyncedFrames frameImage: UIImage, depthInMillimeters: UnsafeMutablePointer<Float>!)
}

class StructureDepthCamera: NSObject {
    static let shared = StructureDepthCamera()
    
    class ImageComposer {
        let context = CIContext()
        
        func image(from stColorFrame: STColorFrame) -> UIImage? {
            guard let sampleBuffer = stColorFrame.sampleBuffer, let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
                return nil
            }
            
            return UIImage(pixelBuffer: pixelBuffer, context: context)
        }
    }
    
    private let imageComposer = ImageComposer()
    
    private struct ColorConfiguration {
        static let useManualExposureAndAutoISO: Bool = true
        static let targetExposureTimeInSeconds: Double = 1.0 / 60
        static let outputDataVideoSettings: [String:Int] = [String(kCVPixelBufferPixelFormatTypeKey): Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)]
        static let lensPosition: Float = 0.75
    }
    
    enum ConnectionStatus {
        case preparing
        case connected
        case failedToFind
        case failedToConnect
        case failedBackgroundNotSupported
        
        case disconnected
        
        init(structureInitStats: STSensorControllerInitStatus) {
            switch structureInitStats {
            case .openFailed:
                self = .failedToConnect
            case .appInBackground:
                self = .failedBackgroundNotSupported
            case .alreadyInitialized, .success:
                self = .connected
            case .sensorNotFound:
                self = .failedToFind
            case .sensorIsWakingUp:
                self = .preparing
            }
        }
        
        var description: String {
            switch self {
            case .connected:
                return "Initialized"
            case .failedToFind:
                return "Uninitialized because the sensor was not found."
            case .preparing:
                return "Uninitialized because sensor is waking up."
            case .failedToConnect:
                return "Failure to open the connection."
            case .failedBackgroundNotSupported:
                return "Uninitialized and sensor is not opened because the application is running in the background."
            case .disconnected:
                return "Sensor disconnected"
            }
        }
    }
    
    weak var delegate: StructureDepthCameraDelegate?
    var performTracking = true
    var extractSyncedFrames = true
    
//    var connectionStatus: ConnectionStatus {
//        return ConnectionStatus(structureInitStats: STSensorController.shared().initializeSensorConnection())
//    }
    
    func connect() -> ConnectionStatus {
        let connectionStatus = ConnectionStatus(structureInitStats: STSensorController.shared().initializeSensorConnection())
        delegate?.structureDepthCamera(self, didUpdateStatus: connectionStatus)
        return connectionStatus
    }
    
    var batteryChargePercentage: Int {
        let connectionStatus = ConnectionStatus(structureInitStats: STSensorController.shared().initializeSensorConnection())
        guard connectionStatus == .connected else {
            NSLog(connectionStatus.description)
            return -1
        }
        
        return Int(STSensorController.shared().getBatteryChargePercentage())
    }
    
    private override init() {
        super.init()
        
        STSensorController.shared().delegate = self
    }
    
    func startStreaming() {
        let connectionStatus = connect()
        guard connectionStatus == .connected && !STSensorController.shared().isLowPower() else {
            return
        }
        do {
            try STSensorController.shared().startStreaming(options: [
                kSTStreamConfigKey:                 STStreamConfig.depth640x480.rawValue,
                kSTFrameSyncConfigKey:              STFrameSyncConfig.depthAndRgb.rawValue,
                kSTColorCameraFixedLensPositionKey: ColorConfiguration.lensPosition])
            
            NSLog("Starting sensor streaming...")
        } catch let error {
            NSLog("[Structure] [ERROR] sensor failed to stream, error: \(error.localizedDescription)")
        }
    }
    
    func pauseStreaming() {
        STSensorController.shared().stopStreaming()
    }
    
    func syncWithDeviceCamera(sampleBuffer: CMSampleBuffer) {
        print("sampleBuffer: \(StructureDepthCamera.shared.delegate)")
        STSensorController.shared().frameSyncNewColorBuffer(sampleBuffer)
    }
    
}

extension StructureDepthCamera: STSensorControllerDelegate {
    func sensorDidConnect() {
        NSLog("sensorDidConnect")
        delegate?.structureDepthCamera(self, didUpdateStatus: ConnectionStatus.connected)
    }
    
    func sensorDidDisconnect() {
        NSLog("sensorDidDisconnect")
        delegate?.structureDepthCamera(self, didUpdateStatus: ConnectionStatus.disconnected)
    }
    
    func sensorDidStopStreaming(_ reason: STSensorControllerDidStopStreamingReason) {
        NSLog("sensorDidStopStreaming: \(reason)")
//        delegate?.structureDepthCamera(self, didInterruptWithStatus: ConnectionStatus.)
    }
    
    func sensorDidLeaveLowPowerMode() {
        NSLog("sensorDidLeaveLowPowerMode")
        delegate?.structureDepthCamera(self, batteryChargePercentage: batteryChargePercentage, isLow: false)
    }
    
    func sensorBatteryNeedsCharging() {
        NSLog("sensorBatteryNeedsCharging")
        delegate?.structureDepthCamera(self, batteryChargePercentage: batteryChargePercentage, isLow: true)
    }
    
    func sensorDidOutputSynchronizedDepthFrame(_ depthFrame: STDepthFrame!, colorFrame: STColorFrame!) {
        guard extractSyncedFrames || performTracking else {
            return
        }
        
        guard let depthFrameCopy = depthFrame.copy() as? STDepthFrame else {
            NSLog("failed to copy depth frame")
            return
        }
        
        // TODO: should we use registeredDepthFrame??
        guard let registeredDepthFrame = depthFrameCopy.registered( to: colorFrame) else {
            NSLog("could not extract registered depth frame")
            return
        }
        
        if extractSyncedFrames {
            guard
                let colorFrame = colorFrame,
                let colorImg = imageComposer.image(from: colorFrame),
                let depthFrame = depthFrame,
                let depthArray = depthFrame.depthInMillimeters
                else {
                    return
            }
            
            delegate?.structureDepthCamera(self, didExtractSyncedFrames: colorImg, depthInMillimeters: depthArray)
        }
        
        if performTracking {
            // call tracker thread
        }
    }
}
