//
//  ModelNotificationView.swift
//  Astralink
//
//  Created by Ido Schragenheim on 04/09/2017.
//  Copyright © 2017 Astralink. All rights reserved.
//

import UIKit

class ModelNotificationView: UIView {

    var headerLabel: UILabel!
    var bodyLabel: UILabel!
    var iconImageView: UIImageView!

    var notification: NotificationData? {
        didSet {
            layoutData()
        }
    }

    init(notification: NotificationData? = nil) {
        headerLabel = UILabel()
        bodyLabel = UILabel()
        iconImageView = UIImageView()

        headerLabel.text = notification?.header
        bodyLabel.text = notification?.body
        iconImageView.image = notification?.icon
        super.init(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
        addSubview(headerLabel)
        addSubview(bodyLabel)
        addSubview(iconImageView)
        configureStyling()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        layoutData()
        super.layoutSubviews()
    }

    func layoutData() {
        headerLabel.text = notification?.header
        bodyLabel.text = notification?.body
        iconImageView.image = notification?.icon
    }

    func configureStyling() {
        bodyLabel.numberOfLines = 3
        bodyLabel.font = UIFont.preferredFont(forTextStyle: .body)

        headerLabel.font = UIFont.preferredFont(forTextStyle: .headline)

        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 8
        backgroundColor = UIColor.white.withAlphaComponent(0.75)

        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            iconImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15),
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            iconImageView.widthAnchor.constraint(equalToConstant: 32),
            iconImageView.heightAnchor.constraint(equalToConstant: 32),
            headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15),
            headerLabel.rightAnchor.constraint(equalTo: iconImageView.leftAnchor, constant: 10),
            bodyLabel.rightAnchor.constraint(equalTo: iconImageView.leftAnchor, constant: 10),
            bodyLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15),
            bodyLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        ]
        NSLayoutConstraint.activate(constraints)
    }

}
