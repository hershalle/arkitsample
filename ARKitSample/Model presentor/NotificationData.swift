//
//  ModelNotification.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 22/01/2018.
//  Copyright © 2018 Shai Balassiano. All rights reserved.
//

import Foundation

struct NotificationData {
    var header: String
    var body: String
    var icon: UIImage
}
