//
//  ViewController2.swift
//  ARKitSample
//
//  Created by Shai Balassiano on 26/12/2017.
//  Copyright © 2017 Shai Balassiano. All rights reserved.
//

import UIKit
import ARKit
import Crashlytics

class ModelViewController: UIViewController {
    @IBOutlet var testImageView: UIImageView!
    private var notificationView = ModelNotificationView()

    // injected:
    var arCameraController: ARCameraController! {
        didSet {
            addChildViewController(arCameraController)
            arCameraController.view.frame = view.frame
            view.addSubviewWithSameSizeConstrainst(subview: arCameraController.view)
            arCameraController.didMove(toParentViewController: self)
            
            arCameraController.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Crashlytics.sharedInstance().throwException()
        setupNotificationView()
//        toggleTestImage()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        arCameraController.start()
    }
    
//    func toggleTestImage() {
//        testImageView.isHidden = arCameraController.arFlow.state != .tracking
//    }
    
    @IBAction func didTap(startFirstAlignmentButton: UIButton) {
//        arCameraController.start()
//        arCameraController.arFlow.state = .firstAlignment
    }
    
    // ======================
    // MARK: - Notifications:
    // ======================
    private func setupNotificationView() {
        notificationView.isHidden = true
        view.addSubview(notificationView)
        
        /// constraints
        notificationView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            notificationView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 30),
            notificationView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            notificationView.widthAnchor.constraint(equalToConstant: 400),
            notificationView.heightAnchor.constraint(equalToConstant: 90)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func showNotification(notification: NotificationData, isSticky: Bool) {
        notificationView.superview?.bringSubview(toFront: notificationView)
        notificationView.notification = notification
        
        notificationView.layer.removeAllAnimations()
        /// Progressively show the message label.
        view.isUserInteractionEnabled = true
        notificationView.alpha = 0
        notificationView.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.notificationView.alpha = 1.0
        }) { (success) in
            guard success else {
                return
            }
            
            if !isSticky {
                self.hideNotification()
            }
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.notificationView.alpha = 1.0
        })
    }
    
    private func hideNotification() {
        notificationView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.7, animations: {
            self.notificationView.alpha = 0
        }, completion: { _ in
            if self.notificationView.notification == nil {
                if self.view != nil {
                    self.notificationView.isHidden = true
                    self.view.isUserInteractionEnabled = true
                }
            }
            
            self.notificationView.notification = nil
        })
    }
}

extension ModelViewController: ARCameraControllerDelegate {
    func arCameraCurrentNotification(_ arCamera: ARCameraController) -> NotificationData? {
        return notificationView.notification
    }
    
    func arCamera(_ arCamera: ARCameraController, show notification: NotificationData, isSticky: Bool) {
        showNotification(notification: notification, isSticky: isSticky)
    }
    
    func arCameraHideStickyNotification(_ arCamera: ARCameraController) {
        hideNotification()
    }
    
    func arCamera(_ arCamera: ARCameraController, didUpdate tracking: GLKMatrix4) {
        
    }
    
    func arCamera(_ arCamera: ARCameraController, didUpdate sampleBuffer: CMSampleBuffer) {
        
    }
}
